package pet;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.restassured.response.ValidatableResponse;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.*;
import pojo.pet.Category;
import pojo.pet.Pet;
import pojo.pet.TagObject;
import utils.actions.PetActions;
import utils.data.JsonHelper;
import utils.data.Util;
import java.util.ArrayList;
import java.util.List;

@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public class PetTest {

    Pet pet;
    int id = Util.generateId();
    String [] validStatuses = {"available", "pending", "sold"};
    String badStatus = "processing";
    int badId = -10;

    @BeforeEach
    void prepare() {
        ArrayList<TagObject> tagList = new ArrayList<TagObject>();
        ArrayList<String> urlList = new ArrayList<String>();
        TagObject tagPojo = JsonHelper.generateJSONForTag(1, "fluffy");
        tagList.add(tagPojo);
        Category category = JsonHelper.generateJSONForCategory(1,"dog");
        urlList.add("/dogs");
        pet = JsonHelper.generateJSONForPet(id,
                "Jack",
                validStatuses[0],
                category,
                urlList,
                tagList
        );
    }

    @Test
    @Order(1)
    void verifyThatPetCouldBeCreatedAndDeleted() {
        PetActions.createPet(pet);
        PetActions.getPetById(id);
        PetActions.deletePet(id);
        PetActions.getPetByNotExistingId(id);
    }

    @Test
    @Order(2)
    void verifyThatPetCouldNotBeCreatedWithEmptyBody() {
        PetActions.createPetWithInvalidData();
    }

    @Test
    @Order(3)
    void verifyThatPetCouldNotBeCreatedWithWrongStatus() {
        pet.setStatus(badStatus);
        PetActions.createPetWithWrongStatus(pet);
        PetActions.deleteNotExistingPet(id);
    }

    @Test
    @Order(4)
    void verifyThatPetCouldNotBeDeletedWithWrongId() {
        PetActions.deleteNotExistingPet(id);
    }

    @Test
    @Order(5)
    void verifyThatPetCouldBeFoundByCorrectStatus() {
        for(String status: validStatuses){
            pet.setStatus(status);
            PetActions.createPet(pet);
            ValidatableResponse response = PetActions.getByStatus(status);
            PetActions.deletePet(id);
            ObjectMapper mapper = new ObjectMapper();
            JsonNode pets = response.extract().body().as(JsonNode.class);
            List<Pet> petList = mapper.convertValue(
                    pets,
                    new TypeReference<List<Pet>>(){}
            );

            Assertions.assertThat(petList.get(0).getStatus())
                    .as("Pet with wrong status was retrieved. Id: " + id)
                    .isEqualTo(status);
        }
    }

    @Test
    @Order(6)
    void verifyThatPetCouldNotBeFoundByWrongStatus() {
        PetActions.getByBadStatus(badStatus);
    }

    @Test
    @Order(7)
    void verifyThatPetCouldNotBeFoundByWrongId() {
        PetActions.getPetByWrongId(badId);
    }

    @Test
    @Order(8)
    void verifyThatNewPetCouldNotBePut() {
        PetActions.updateNonExistingPet(pet);
    }

    @Test
    @Order(9)
    void verifyThatExistingPetCouldBeUpdatedByPutRequestWithStatus() {
        PetActions.createPet(pet);
        pet.setStatus(validStatuses[2]);
        PetActions.updateExistingPet(pet);
        ValidatableResponse response = PetActions.getPetById(id);
        String actualStatus = response.extract().body().as(Pet.class).getStatus();
        Assertions.assertThat(actualStatus).as("Status was not updated for id: " + id).isEqualTo(validStatuses[2]);
    }

    @Test
    @Order(10)
    void verifyThatExistingPetCouldBeUpdatedByPutRequestWithCategory() {
        PetActions.createPet(pet);
        Category categoryUpdate = JsonHelper.generateJSONForCategory(2,"cat");
        pet.setCategory(categoryUpdate);
        PetActions.updateExistingPet(pet);
        ValidatableResponse response = PetActions.getPetById(id);
        Category actualCategory = response.extract().body().as(Pet.class).getCategory();
        Assertions.assertThat(actualCategory.getId()).as("Category ID was not updated for pet: " + id).isEqualTo(categoryUpdate.getId());
        Assertions.assertThat(actualCategory.getName()).as("Category NAME was not updated for pet: " + id).isEqualTo(categoryUpdate.getName());
    }

    @Test
    @Order(11)
    void verifyThatExistingPetCouldBeUpdatedByPutRequestWithName() {
        String name = "John";
        PetActions.createPet(pet);
        pet.setName(name);
        PetActions.updateExistingPet(pet);
        ValidatableResponse response = PetActions.getPetById(id);
        String actualName = response.extract().body().as(Pet.class).getName();
        Assertions.assertThat(actualName).as("Name was not updated for pet: " + id).isEqualTo(name);
    }

    @Test
    @Order(12)
    void verifyThatExistingPetCouldBeUpdatedByPutRequestWithPhotoUrl() {
        PetActions.createPet(pet);
        ArrayList<String> urlListNew = new ArrayList<String>();
        urlListNew.add("/cat");
        pet.setPhotoUrls(urlListNew);
        PetActions.updateExistingPet(pet);
        ValidatableResponse response = PetActions.getPetById(id);
        ArrayList<String> actualPhotoUrl = response.extract().body().as(Pet.class).getPhotoUrls();
        Assertions.assertThat(actualPhotoUrl).as("Photo URL was not updated for id: " + id).isEqualTo(urlListNew);
    }

    @Test
    @Order(13)
    void verifyThatExistingPetCouldBeUpdatedByPutRequestWithTag() {
        PetActions.createPet(pet);
        ArrayList<TagObject> tagList = new ArrayList<TagObject>();
        TagObject tagPojo = JsonHelper.generateJSONForTag(2, "super fluffy");
        tagList.add(tagPojo);
        pet.setTags(tagList);
        PetActions.updateExistingPet(pet);
        ValidatableResponse response = PetActions.getPetById(id);
        ArrayList<TagObject> actualTag = response.extract().body().as(Pet.class).getTags();
        Assertions.assertThat(actualTag.get(0).getId()).as("Tag ID was not updated for id: " + id).isEqualTo(tagPojo.getId());
        Assertions.assertThat(actualTag.get(0).getName()).as("Tag NAME was not updated for id: " + id).isEqualTo(tagPojo.getName());
    }

    @Test
    @Order(14)
    void verifyThatPetCouldBePutWithWrongId() {
        pet.setId(badId);
        PetActions.updatePetWithInvalidId(pet);
    }

    @Test
    @Order(15)
    void verifyThatPetCouldBePutWithWrongStatus() {
        PetActions.createPet(pet);
        pet.setStatus(badStatus);
        PetActions.updatePetWithInvalidStatus(pet);
    }

    @Test
    @Order(16)
    void verifyThatImageOfPetCouldBeUploaded() {
        PetActions.uploadImageOfPet(id);
    }

    @Test
    @Order(17)
    void verifyThatPetCouldBeUpdatedWithFormData() {
        String expectedName = "John";
        String expectedStatus = "sold";
        PetActions.createPet(pet);
        PetActions.updatePetWithFormData(id, expectedName, expectedStatus );
        ValidatableResponse response = PetActions.getPetById(id);
        String actualName = response.extract().body().as(Pet.class).getName();
        String actualStatus = response.extract().body().as(Pet.class).getStatus();
        Assertions.assertThat(actualName).as("Pet name was not updated for id: " + id).isEqualTo(expectedName);
        Assertions.assertThat(actualStatus).as("Pet status was not updated for id: " + id).isEqualTo(expectedStatus);
    }

    @Test
    @Order(18)
    void verifyThatPetCouldNotBeUpdatedWithFormDataAndNonExistingId() {
        PetActions.updatePetWithFormDataAndNonExistingId(id ,"John", "sold");
    }

    @Test
    @Order(19)
    void verifyThatPetCouldNotBeUpdatedWithFormDataAndBadStatus() {
        PetActions.createPet(pet);
        PetActions.updatePetWithFormDataAndBadId(id ,"John", badStatus);
    }
}
