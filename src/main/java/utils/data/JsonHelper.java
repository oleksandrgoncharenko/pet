package utils.data;

import pojo.pet.Category;
import pojo.pet.Pet;
import pojo.pet.TagObject;

import java.util.ArrayList;

public class JsonHelper {
    public static Pet generateJSONForPet(int id,
                                         String name,
                                         String status,
                                         Category category,
                                         ArrayList<String> photoUrls,
                                         ArrayList<TagObject> tags) {

        Pet petPOJO = new Pet()
                .setId(id)
                .setName(name)
                .setStatus(status)
                .setCategory(category)
                .setPhotoUrls(photoUrls)
                .setTags(tags);

        return petPOJO;
    }

    public static Category generateJSONForCategory(int id, String name) {

        Category categoryPOJO = new Category()
                .setId(id)
                .setName(name);

        return categoryPOJO;
    }

    public static TagObject generateJSONForTag(int id, String name) {

        TagObject tagPOJO = new TagObject()
                .setId(id)
                .setName(name);

        return tagPOJO;
    }
}
