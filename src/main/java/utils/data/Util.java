package utils.data;

import java.text.SimpleDateFormat;
import java.util.Date;

public class Util {
    public static int generateId(){
        return Math.abs(new SimpleDateFormat("dd-MM-yyyy HH:mm:ss")
                        .format(new Date())
                        .hashCode());
    }
}
