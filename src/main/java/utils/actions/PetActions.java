package utils.actions;

import io.restassured.http.ContentType;
import io.restassured.path.json.JsonPath;
import io.restassured.response.ValidatableResponse;
import pojo.pet.Pet;
import utils.api.CommonRestMethods;
import utils.api.EndPoints;
import org.assertj.core.api.Assertions;
import utils.api.PetRestMethods;

public class PetActions {

    private static void assertResponseCodeAndContentType(ValidatableResponse response, int code, String message) {
        Assertions.assertThat(response.extract().statusCode()).as(message).isEqualTo(code);
        Assertions.assertThat(response.extract().contentType()).as("Response is not JSON format").isEqualTo(ContentType.JSON.toString());
    }

    private static void assertResponseMessageValue(ValidatableResponse response, int id) {
        String message = JsonPath.from(response.extract().body().asString()).get("message");
        Assertions.assertThat(message).as("Wrong pet was deleted/updated").isEqualTo(String.valueOf(id));
    }

    public static ValidatableResponse createPet(Pet petPOJO) {
        ValidatableResponse response = CommonRestMethods.post(EndPoints.petPostPut, petPOJO);
        PetActions.assertResponseCodeAndContentType(response, 200, "Pet was not created.");
        return response;
    }

    public static ValidatableResponse createPetWithInvalidData() {
        ValidatableResponse response = CommonRestMethods.postWithEmptyBody(EndPoints.petPostPut);
        PetActions.assertResponseCodeAndContentType(response, 405, "Pet with empty body should not be created");
        return response;
    }

    public static ValidatableResponse createPetWithWrongStatus(Pet petPOJO) {
        ValidatableResponse response = CommonRestMethods.post(EndPoints.petPostPut, petPOJO);
        PetActions.assertResponseCodeAndContentType(response, 405, "Pet with invalid status should not be created.Status: " + petPOJO.getStatus());
        return response;
    }

    public static ValidatableResponse getPetById(int id) {
        ValidatableResponse response = CommonRestMethods.getById(EndPoints.petById, id);
        PetActions.assertResponseCodeAndContentType(response, 200, "Pet was not found by id: " + id);
        return response;
    }

    public static ValidatableResponse getPetByNotExistingId(int id) {
        ValidatableResponse response = CommonRestMethods.getById(EndPoints.petById, id);
        PetActions.assertResponseCodeAndContentType(response, 404, "Pet should not be found with not existing id: " + id);
        return response;
    }

    public static ValidatableResponse getPetByWrongId(int id) {
        ValidatableResponse response = CommonRestMethods.getById(EndPoints.petById, id);
        PetActions.assertResponseCodeAndContentType(response, 400, "Pet should not be found with wrong id: " + id);
        return response;
    }

    public static ValidatableResponse deletePet(int id) {
        ValidatableResponse response = CommonRestMethods.delete(EndPoints.petById, id);
        PetActions.assertResponseCodeAndContentType(response, 200, "Pet was not deleted. Id: " + id);
        PetActions.assertResponseMessageValue(response, id);
        return response;
    }

    public static ValidatableResponse deleteNotExistingPet(int id) {
        ValidatableResponse response = CommonRestMethods.delete(EndPoints.petById, id);
        PetActions.assertResponseCodeAndContentType(response, 404, "Pet should not be deleted by not existing id: " + id);
        return response;
    }

    public static ValidatableResponse getByStatus(String status) {
        ValidatableResponse response = PetRestMethods.getPetByStatus(EndPoints.petFindByStatus, status);
        PetActions.assertResponseCodeAndContentType(response, 200, "Pet was not found by status: " + status);
        return response;
    }

    public static ValidatableResponse getByBadStatus(String status) {
        ValidatableResponse response = PetRestMethods.getPetByStatus(EndPoints.petFindByStatus, status);
        PetActions.assertResponseCodeAndContentType(response, 400, "Pet should not be found by status: " + status);
        return response;
    }

    public static ValidatableResponse updateExistingPet(Pet petPOJO) {
        ValidatableResponse response = CommonRestMethods.post(EndPoints.petPostPut, petPOJO);
        PetActions.assertResponseCodeAndContentType(response, 200, "Pet was not updated: " + petPOJO.getId());
        return response;
    }

    public static ValidatableResponse updateNonExistingPet(Pet petPOJO) {
        ValidatableResponse response = CommonRestMethods.post(EndPoints.petPostPut, petPOJO);
        PetActions.assertResponseCodeAndContentType(response, 404, "Non existing pet should not be updated: " + petPOJO.getId());
        return response;
    }

    public static ValidatableResponse updatePetWithInvalidId(Pet petPOJO) {
        ValidatableResponse response = CommonRestMethods.post(EndPoints.petPostPut, petPOJO);
        PetActions.assertResponseCodeAndContentType(response, 400, "Pet should not be updated by invalid id: " + petPOJO.getId());
        return response;
    }

    public static ValidatableResponse updatePetWithInvalidStatus(Pet petPOJO) {
        ValidatableResponse response = CommonRestMethods.post(EndPoints.petPostPut, petPOJO);
        PetActions.assertResponseCodeAndContentType(response, 405, "Pet should not be updated by invalid status: " + petPOJO.getStatus());
        return response;
    }

    public static ValidatableResponse uploadImageOfPet(int id) {
        ValidatableResponse response = PetRestMethods.postUploadImage(EndPoints.uploadImage, id);
        PetActions.assertResponseCodeAndContentType(response, 200, "Image was not uploaded for id: " + id);
        return response;
    }

    public static ValidatableResponse updatePetWithFormData(int id, String name, String status) {
        ValidatableResponse response = PetRestMethods.postWithFormData(EndPoints.petById, id, name, status);
        PetActions.assertResponseCodeAndContentType(response, 200, "Pet was not updated with form data for id: " + id);
        PetActions.assertResponseMessageValue(response, id);
        return response;
    }

    public static ValidatableResponse updatePetWithFormDataAndNonExistingId(int id, String name, String status) {
        ValidatableResponse response = PetRestMethods.postWithFormData(EndPoints.petById, id, name, status);
        PetActions.assertResponseCodeAndContentType(response, 404, "Pet should not be updated by non existing id: " + id);
        return response;
    }

    public static ValidatableResponse updatePetWithFormDataAndBadId(int id, String name, String status) {
        ValidatableResponse response = PetRestMethods.postWithFormData(EndPoints.petById, id, name, status);
        PetActions.assertResponseCodeAndContentType(response, 405, "Pet should not be updated by non bad id: " + id);
        return response;
    }
}
