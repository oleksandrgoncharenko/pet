package utils.api;

import io.restassured.http.ContentType;
import io.restassured.response.ValidatableResponse;
import pojo.Pojo;

import java.util.HashMap;

import static io.restassured.RestAssured.given;

public class PetRestMethods {

    public static ValidatableResponse getPetByStatus(String endPoint, String status) {
        ValidatableResponse response = given().
                spec(RequestSpecifications.specWithOutKey).
                when().
                param("status", status).
                get(endPoint).
                then();

        return response;
    }

    public static ValidatableResponse postUploadImage(String endPoint, int id) {
        ValidatableResponse response = given().
                spec(RequestSpecifications.specForUploadImage).
                when().
                post(endPoint, id).
                then();

        return response;
    }

    public static ValidatableResponse postWithFormData(String endPoint, int id, String name, String status) {
        HashMap<String, String> map = new HashMap<String, String>();
        map.put("name", name);
        map.put("status", status);
        ValidatableResponse response = given().
                formParams(map).
                spec(RequestSpecifications.specUpdateWithFormData).
                when().
                post(endPoint, id).
                then();

        return response;
    }




}
