package utils.api;

import io.restassured.response.ValidatableResponse;
import pojo.Pojo;

import static io.restassured.RestAssured.given;

public class CommonRestMethods {
    public static ValidatableResponse get(String endPoint) {
        ValidatableResponse response = given().
                spec(RequestSpecifications.specWithOutKey).
                when().
                get(endPoint).
                then();

        return response;
    }

    public static ValidatableResponse getById(String endPoint, int id) {
        ValidatableResponse response = given().
                spec(RequestSpecifications.specWithOutKey).
                when().
                get(endPoint, id).
                then();

        return response;
    }

    public static ValidatableResponse post(String endPoint, Pojo body) {
        ValidatableResponse response = given().
                spec(RequestSpecifications.specWithOutKey).
                body(body).
                when().
                post(endPoint).
                then();

        return response;
    }

    public static ValidatableResponse postWithEmptyBody(String endPoint) {
        ValidatableResponse response = given().
                spec(RequestSpecifications.specWithOutKey).
                when().
                post(endPoint).
                then();

        return response;
    }

    public static ValidatableResponse put(String endPoint, Pojo body) {
        ValidatableResponse response = given().
                spec(RequestSpecifications.specWithOutKey).
                body(body).
                when().
                post(endPoint).
                then();

        return response;
    }

    public static ValidatableResponse delete(String endPoint, int id) {
        ValidatableResponse response = given().
                spec(RequestSpecifications.specWithKey).
                when().
                delete(endPoint, id).
                then();

        return response;
    }


}
