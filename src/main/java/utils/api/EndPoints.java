package utils.api;


public final class EndPoints {
    public final static String uploadImage = "/pet/{petId}/uploadImage";
    public final static String petPostPut = "/pet";
    public final static String petFindByStatus = "/pet/findByStatus";
    public final static String petById = "/pet/{petId}";
}
