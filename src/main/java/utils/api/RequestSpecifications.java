package utils.api;

import io.restassured.builder.RequestSpecBuilder;
import io.restassured.http.ContentType;
import io.restassured.specification.RequestSpecification;

import java.io.File;

public class RequestSpecifications {
    private static String baseURL = "https://petstore.swagger.io/v2";
    private static String apiKey = "petstore2021";


    public static RequestSpecification specForUploadImage = new RequestSpecBuilder()
            .setBaseUri(baseURL)
            .setAccept(ContentType.JSON)
            .addMultiPart("file", new File("src/main/resources/TestPost.jpg"))
            .addMultiPart("additionalMetadata", "meta")
            .build();

    public static RequestSpecification specWithKey = new RequestSpecBuilder()
            .setBaseUri(baseURL)
            .setAccept(ContentType.JSON)
            .setContentType(ContentType.JSON)
            .addHeader("api_key", apiKey)
            .build();

    public static RequestSpecification specWithOutKey = new RequestSpecBuilder()
            .setBaseUri(baseURL)
            .setAccept(ContentType.JSON)
            .setContentType(ContentType.JSON)
            .build();

    public static RequestSpecification specUpdateWithFormData = new RequestSpecBuilder()
            .setBaseUri(baseURL)
            .setAccept(ContentType.JSON)
            .addHeader("content-type", "application/x-www-form-urlencoded")
            .build();
}
