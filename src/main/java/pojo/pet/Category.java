package pojo.pet;

import pojo.Pojo;

public class Category implements Pojo {
    private int id;
    private String name;


    // Getter Methods

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    // Setter Methods

    public Category setId(int id) {
        this.id = id;
        return this;
    }

    public Category setName(String name) {
        this.name = name;
        return this;
    }
}
