package pojo.pet;

import pojo.Pojo;

import java.util.ArrayList;

public class Pet implements Pojo {
    private int id;
    Category category;
    private String name;
    ArrayList < String > photoUrls;
    ArrayList <TagObject> tags;


    private String status;


    // Getter Methods

    public int getId() {
        return id;
    }

    public ArrayList<String> getPhotoUrls() {
        return photoUrls;
    }

    public ArrayList<TagObject> getTags() {
        return tags;
    }

    public Category getCategory() {
        return category;
    }

    public String getName() {
        return name;
    }

    public String getStatus() {
        return status;
    }

    // Setter Methods

    public Pet setId(int id) {
        this.id = id;
        return this;
    }

    public Pet setCategory(Category category) {
        this.category = category;
        return this;
    }

    public Pet setName(String name) {
        this.name = name;
        return this;
    }

    public Pet setStatus(String status) {
        this.status = status;
        return this;
    }

    public Pet setTags(ArrayList<TagObject> tags) {
        this.tags = tags;
        return this;
    }

    public Pet setPhotoUrls(ArrayList<String> photoUrls) {
        this.photoUrls = photoUrls;
        return this;
    }
}
