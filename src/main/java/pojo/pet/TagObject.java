package pojo.pet;

public class TagObject {
    private int id;
    private String name;


    // Getter Methods

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    // Setter Methods

    public TagObject setId(int id) {
        this.id = id;
        return this;
    }

    public TagObject setName(String name) {
        this.name = name;
        return this;
    }
}
