# HOW TO




Test automation framework for testing of PET Service

## Testing


### Run tests locally  
The following command will run tests. These tests must be in classes named ```*Test.java```

``` mvn test -Dtest=*Test```  

### Generate report locally  
The following commands will generate css for report, run tests and generate report itself here ```target/site```.

``` mvn site -DgenerateReports=false```

``` mvn surefire-report:report```  

### Run tests CI  
Automatically pipeline (build->test) is running automatically after each commit to ```master```
If you want to run it manually, `Run pipeline` from CI/CD tab: https://gitlab.com/oleksandrgoncharenko/pet/-/pipelines

### Generate report on CI  
Reports are generated automatically. You can find it here: CI/CD=>Pipelines=>Pipeline=>Tests tab (ex.: https://gitlab.com/oleksandrgoncharenko/pet/-/pipelines/288063109)

  

  
